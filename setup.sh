#!/bin/bash


echo -e "\n\n#######  updating packages #######\n\n"

sudo apt update

sudo DEBIAN_FRONTEND=noninteractive apt upgrade -y

echo -e "\n\n#######  installing common packages  #######\n\n"

sudo apt install software-properties-common

echo -e "\n\n#######  installing ansible  #######\n\n"

sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y


sudo ansible-playbook setup.yml 
